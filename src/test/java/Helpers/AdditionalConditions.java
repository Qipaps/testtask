package Helpers;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;

import java.util.ArrayList;
import java.util.List;

public class AdditionalConditions  extends BasePage{

    public ExpectedCondition<Boolean> elementFound(By locator){
        return new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver webDriver) {
                WebElement el = driver.findElement(locator);
                return true;
            }
        };
    }

    public static ExpectedCondition<Boolean> getElements(By locator, ArrayList<WebElement> listElements) {
        return new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver driver) {
                List<WebElement> listElementsTmp = driver.findElements(locator);
                for(int i = 0; i < listElementsTmp.size(); i++){
                    listElements.add(listElementsTmp.get(i));
                }
                return true;
            }
        };
    }

    public static ExpectedCondition<Boolean> getTextOfElements(By locator, ArrayList<String> listText) {
        return new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver driver) {
                List<WebElement> listElements = driver.findElements(locator);
                for(int i = 0; i < listElements.size(); i++){
                    listText.add(listElements.get(i).getText());
                }
                return true;
            }
        };
    }

    public static ExpectedCondition<Boolean> getCountOfElements(By locator) {
        return new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver driver) {
                List<WebElement> listElements = driver.findElements(locator);
                System.out.println(listElements.size());
                return true;
            }
        };
    }

    public static ExpectedCondition<Boolean> getAttribute(By locator,  String attribute) {
        return new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver driver) {
                WebElement el = driver.findElement(locator);
                System.out.println(el.getAttribute(attribute));
                return true;
            }
        };
    }

    public static ExpectedCondition<Boolean> getSizeElement(By locator) {
        return new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver driver) {
                WebElement el = driver.findElement(locator);
                System.out.println("Height: " + el.getSize().height + " Width: " + el.getSize().width);
                return true;
            }
        };
    }

    public static ExpectedCondition<Boolean> getPositionElement(By locator) {
        return new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver driver) {
                WebElement el = driver.findElement(locator);
                System.out.println("X: " + el.getLocation().getX() + "; Y: " + el.getLocation().getY());
                return true;
            }
        };
    }

    public static ExpectedCondition<Boolean> ClickElement(WebElement el) {
        return new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver driver) {
                el.click();
                return true;
            }
        };
    }


    public static ExpectedCondition<Boolean> elementFoundAndClicked(By locator) {
        return new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver driver) {
                WebElement el = driver.findElement(locator);
                el.click();
                return true;
            }
        };
    }

    public static ExpectedCondition sendKeys(By element, String s) {
        return new ExpectedCondition() {
            @Override
            public Object apply(Object o) {
                WebElement el = driver.findElement(element);
                el.sendKeys(s);
                return true;
            }
        };
    }

    public static ExpectedCondition clearText(By element) {
        return new ExpectedCondition() {
            @Override
            public Object apply(Object o) {
                WebElement el = driver.findElement(element);
                el.clear();
                return true;
            }
        };
    }

    public static ExpectedCondition getTextElement(By element) {
        return new ExpectedCondition() {
            @Override
            public Object apply(Object o) {
                WebElement el = driver.findElement(element);
                System.out.println(el.getText());
                return true;
            }
        };
    }

    public static ExpectedCondition getValueElement(By element) {
        return new ExpectedCondition() {
            @Override
            public Object apply(Object o) {
                WebElement el = driver.findElement(element);
                System.out.println(el.getAttribute("Value"));
                return true;
            }
        };
    }

    public static ExpectedCondition getStatusEnable(By element) {
        return new ExpectedCondition() {
            @Override
            public Object apply(Object o) {
                WebElement el = driver.findElement(element);
                if(el.isEnabled()){
                    System.out.println("isEnable");
                }else {
                    System.out.println("isNotEnable");
                }
                return true;
            }
        };
    }

}
