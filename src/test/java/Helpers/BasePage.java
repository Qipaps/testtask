package Helpers;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

public class BasePage {

    protected static WebDriver driver;
    protected static WebDriverWait wait;


    public BasePage(){
        System.setProperty("webdriver.chrome.driver", "D://Project//Driver//chromedriver.exe");
        driver = new ChromeDriver();
        wait = new WebDriverWait(driver, 60);
    }
}
