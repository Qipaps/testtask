package Pages;

import Helpers.BasePage;
import org.openqa.selenium.By;

import static Helpers.AdditionalConditions.*;

public class ButtonPage extends BasePage {

    private By ClickButton = By.id("home");
    private By PositionButton = By.id("position");
    private By ColorButton = By.id("color");
    private By SizeButton = By.id("size");

    public  ButtonPage(){
        driver.navigate().to("http://www.leafground.com/pages/Button.html");
    }

    public ButtonPage ClickButton(){
        wait.until(elementFoundAndClicked(ClickButton));
        if(driver.getCurrentUrl().contains("http://www.leafground.com/home.html")){
            System.out.println(driver.getCurrentUrl());
            driver.navigate().to("http://www.leafground.com/pages/Button.html");
        }else{
            System.out.println(driver.getCurrentUrl());
        }
        return this;
    }

    public ButtonPage GetPositionOfButton(){
        wait.until(getPositionElement(PositionButton));
        return this;
    }

    public ButtonPage GetColorButton(){
        wait.until(getAttribute(ColorButton, "style"));
        return this;
    }

    public ButtonPage GetSizeButton(){
        wait.until(getSizeElement(SizeButton));
        return this;
    }
}
