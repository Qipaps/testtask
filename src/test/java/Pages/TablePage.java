package Pages;

import Helpers.BasePage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.ArrayList;

import static Helpers.AdditionalConditions.*;

public class TablePage extends BasePage {

    private By ColumnsTable = By.xpath("//*[@id='table_id']/tbody/tr/th");
    private By RowsTable = By.xpath("//*[@id='table_id']/tbody/tr");
    private By ProgressTable = By.xpath("//*[@id='table_id']/tbody/tr[3]/td[2]");
    private By ValueElementTable = By.xpath("//*[@id='table_id']/tbody/tr/td[2]");
    private By CheckTable = By.xpath("//*[@id='table_id']/tbody/tr/td[3]/input");

    public TablePage(){
        driver.navigate().to("http://www.leafground.com/pages/table.html");
    }

    public TablePage GetNumberOfColumns(){
        System.out.print("Number of columns: ");
        wait.until(getCountOfElements(ColumnsTable));
        return this;
    }
    public TablePage GetNumberOfRows(){
        System.out.print("Number of rows: ");
        wait.until(getCountOfElements(RowsTable));
        return this;
    }
    public TablePage GetProgress(){
        System.out.print("Progress value: ");
        wait.until(getTextElement(ProgressTable));
        return this;
    }

    public  TablePage CheckLeastProgress(){
        int index = 0;

        ArrayList<String> listText = new ArrayList<String>();
        ArrayList<WebElement> listElements = new ArrayList<WebElement>();
        wait.until(getTextOfElements(ValueElementTable, listText));
        wait.until(getElements(CheckTable, listElements));
        if(listText.size() > 0){
            int min = Integer.parseInt(listText.get(0).substring(0, listText.get(0).length() - 1));
            for(int i = 1; i < listText.size(); i++){
                if(min > Integer.parseInt(listText.get(i).substring(0, listText.get(i).length() - 1))){
                    min = Integer.parseInt(listText.get(i).substring(0, listText.get(i).length() - 1));
                    index = i;
                }
            }
        }
        wait.until(ClickElement(listElements.get(index)));



        return this;
    }

}
