package Pages;

import Helpers.BasePage;
import org.openqa.selenium.By;

import static Helpers.AdditionalConditions.*;

public class EditPage extends BasePage {

    private By emailField = By.id("email");
    private By appendField = By.xpath("//input[@value='Append ']");
    private By testLeafFiled = By.xpath("//*[@value=\"TestLeaf\"]");
    private By clearField = By.xpath("//*[@value=\"Clear me!!\"]");
    private By enableField = By.xpath("//input[@disabled]");

    public EditPage(){
        driver.navigate().to("http://www.leafground.com/pages/Edit.html");
    }

    public EditPage EnterEmail(){
        wait.until((clearText(emailField)));
        wait.until(sendKeys(emailField, "name@main.com"));
        return this;
    }

    public EditPage AppendText(){
        wait.until(sendKeys(appendField, "(new Text)"));
        return this;
    }

    public EditPage GetFieldText(){
        System.out.print("Default text entered: ");
        wait.until(getValueElement(testLeafFiled));
        return this;
    }

    public EditPage ClearTextFromField(){
        wait.until(clearText(clearField));
        return this;
    }

    public EditPage ConfirmDisable(){
        wait.until(getStatusEnable(enableField));
        return this;
    }



}
